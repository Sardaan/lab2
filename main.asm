section .data
	msg: db "No word", 0 

global _start
section .text
%include "colon.inc"
%include "word.inc"

extern read_word
extern find_word
extern string_length
extern print_string 
extern print_newline

_start: 
	mov rsi, 255
	sub rsp, 256
	mov rdi, rsp
	call read_word
	cmp rax, 0
	je .false
	mov rdi, rax 
	mov rsi, next
	call find_word
	cmp rax, 0 
	je .false	
	add rax, 8
	mov r10, rax
	mov rdi, rax
	call string_length
	add r10, rax
	inc r10
	mov rdi, r10
	mov r13, 1
	call print_string
.exit:
	call print_newline
	mov rax, 60
	syscall
.false:
	mov rdi, msg
	call string_length
	mov rsi, rax
	mov r13, 2
	call print_string
	jmp .exit

