global find_word
extern string_equals
%include "colon.inc"

section .text

find_word:

.loop:
	test rsi, rsi
	jz .exit_false
	push rdi
	push rsi
	add rsi, 8
	call string_equals
	pop rsi
	pop rdi
	test rax, rax
	jnz .exit_true
	mov rsi, [rsi]
	jmp .loop
.exit_true:
	mov rax, rsi 
	ret
.exit_false:
	xor rax, rax 
	ret
